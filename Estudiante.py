from db import db
 
class Estudiante(db.Model):
    
    __tablename__='estudiante'
    
    id=db.Column(db.Integer, primary_hey=True)
    
    nombre=db.Column(db.string(50))
    email=db.Column(db.string(70))
    codigo=db.Column(db.string(15))
    
    def __init__(self, nombre, email, codigo):
        
        self.nombre=nombre
        self.email=email
        self.codigo=codigo
        